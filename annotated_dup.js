var annotated_dup =
[
    [ "AudioHandler", "struct_audio_handler.html", "struct_audio_handler" ],
    [ "AudioSelector", "struct_audio_selector.html", "struct_audio_selector" ],
    [ "bGetValues", "structb_get_values.html", "structb_get_values" ],
    [ "ProbeData_Typedef", "union_probe_data___typedef.html", "union_probe_data___typedef" ],
    [ "puControlTypedef", "structpu_control_typedef.html", "structpu_control_typedef" ],
    [ "UVC_HandleTypeDef", "struct_u_v_c___handle_type_def.html", "struct_u_v_c___handle_type_def" ],
    [ "WaveHandler", "struct_wave_handler.html", "struct_wave_handler" ]
];