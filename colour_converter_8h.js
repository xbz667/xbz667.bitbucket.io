var colour_converter_8h =
[
    [ "colourConverter_Init", "colour_converter_8h.html#ad8aa19656549b5e54dbc4c6f29c221e9", null ],
    [ "getDisplayValues", "colour_converter_8h.html#a09fdc5ab6ccd8a97f1a083cd07251dc9", null ],
    [ "getMeanValue", "colour_converter_8h.html#a1e9d58b4cad06a5cdd792288111967af", null ],
    [ "resetMeanVals", "colour_converter_8h.html#af1e3348ad7425e7391fcf11dec2729d3", null ],
    [ "saturate", "colour_converter_8h.html#a6a6472d8fe10739d598dfb93a4c2859a", null ],
    [ "ycbcr2rgb_HDTV", "colour_converter_8h.html#a9507b9263c12ab1f4d33af8171d47eb3", null ],
    [ "ycbcr2rgb_SDTV", "colour_converter_8h.html#ac9418dc9f34d92b00105dce26ddd43db", null ],
    [ "ycbcr2rgb_standard", "colour_converter_8h.html#a468b1817ed9d23d46a33944acf1b2e41", null ]
];