var searchData=
[
  ['backlight_5fcomp',['backlight_comp',['../structb_get_values.html#a4b5d2b541e394f60f5bb6440ef80ca4f',1,'bGetValues']]],
  ['bgetvalues',['bGetValues',['../structb_get_values.html',1,'']]],
  ['brightness',['brightness',['../structb_get_values.html#ace31b095fe77a4d4e0d3c66538807985',1,'bGetValues']]],
  ['bsp_5faudio_5fout_5fhalftransfer_5fcallback',['BSP_AUDIO_OUT_HalfTransfer_CallBack',['../audio_thread_8h.html#a729c33c16c93296de9eb18a8a9f741c7',1,'audioThread.c']]],
  ['bsp_5faudio_5fout_5ftransfercomplete_5fcallback',['BSP_AUDIO_OUT_TransferComplete_CallBack',['../audio_thread_8h.html#af7880d05162512445bf68d4fa9d303e2',1,'audioThread.c']]],
  ['bytesread',['bytesRead',['../struct_wave_handler.html#aaa6d06c7323523dd83c4a4f2b7d01217',1,'WaveHandler']]]
];
