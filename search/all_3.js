var searchData=
[
  ['cfg_5fstate',['cfg_state',['../struct_u_v_c___handle_type_def.html#afc51999d72b037ce9a8604f14b695f75',1,'UVC_HandleTypeDef']]],
  ['colourconverter_2eh',['colourConverter.h',['../colour_converter_8h.html',1,'']]],
  ['colourconverter_5finit',['colourConverter_Init',['../colour_converter_8h.html#ad8aa19656549b5e54dbc4c6f29c221e9',1,'colourConverter.c']]],
  ['contrast',['contrast',['../structb_get_values.html#a4c6a47fd0d724c26246537e96a05c41a',1,'bGetValues']]],
  ['ctl_5fstate',['ctl_state',['../struct_u_v_c___handle_type_def.html#a9fdbfe20e001b78fe72b3cab14846921',1,'UVC_HandleTypeDef']]],
  ['currentsector',['currentSector',['../struct_wave_handler.html#a12317a8320846fa34578920330756784',1,'WaveHandler']]],
  ['curval',['curVal',['../structpu_control_typedef.html#a158b7505ad397346f7cf13cbc3d4726e',1,'puControlTypedef']]]
];
