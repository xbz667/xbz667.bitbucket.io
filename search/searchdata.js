var indexSectionsWithContent =
{
  0: "_abcdefgilmprstuwy",
  1: "abpuw",
  2: "acrsuw",
  3: "_abcgiprsuwy",
  4: "bcdefgilmps",
  5: "u",
  6: "u",
  7: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Pages"
};

