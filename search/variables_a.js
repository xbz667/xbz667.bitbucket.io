var searchData=
[
  ['saturation',['saturation',['../structb_get_values.html#a4125fce2ae599832fcdd6903d31cca35',1,'bGetValues']]],
  ['sector_5fsize',['sector_size',['../struct_wave_handler.html#a7d37def484362c6e97a2d75144080b1d',1,'WaveHandler']]],
  ['selection',['selection',['../struct_audio_selector.html#af190e8c7f8c6caf004f782b3aa7b5021',1,'AudioSelector']]],
  ['selector',['selector',['../struct_wave_handler.html#a7cc715de833a0f4d62132e96541ee133',1,'WaveHandler']]],
  ['setparam_5fstate',['setParam_state',['../struct_u_v_c___handle_type_def.html#aad70395b92a87dcac058ff222f168616',1,'UVC_HandleTypeDef']]],
  ['sharpness',['sharpness',['../structb_get_values.html#a7d64ee1b05707f3c615d239a9330d8f5',1,'bGetValues']]],
  ['state',['state',['../struct_u_v_c___handle_type_def.html#aa5a1e131bc9b4c021a54ffb1b687e3dc',1,'UVC_HandleTypeDef']]],
  ['stream_5faltsetting',['stream_altSetting',['../struct_u_v_c___handle_type_def.html#a53ea2237c8deaacffb3d037fb49bbee2',1,'UVC_HandleTypeDef']]],
  ['stream_5finterface',['stream_interface',['../struct_u_v_c___handle_type_def.html#ac82570b8bd6991d8ce7cf4ad3c107dde',1,'UVC_HandleTypeDef']]],
  ['streamep',['streamEp',['../struct_u_v_c___handle_type_def.html#a953e438899d57fd11330340901b74efa',1,'UVC_HandleTypeDef']]],
  ['streamep_5faddr',['streamEp_addr',['../struct_u_v_c___handle_type_def.html#a6dfd6a5e44a1ce6de700fe3ae5dc7f9c',1,'UVC_HandleTypeDef']]],
  ['streamlength',['streamLength',['../struct_u_v_c___handle_type_def.html#a377f46fef1ef0309ae88034df6f10792',1,'UVC_HandleTypeDef']]],
  ['streamneg_5fstate',['streamNeg_state',['../struct_u_v_c___handle_type_def.html#a6285d36188e3f92697c630cac622f293',1,'UVC_HandleTypeDef']]],
  ['streampipe',['streamPipe',['../struct_u_v_c___handle_type_def.html#a848d8a9aa16cdb7b2340cee9cae72b58',1,'UVC_HandleTypeDef']]],
  ['streampoll',['streamPoll',['../struct_u_v_c___handle_type_def.html#aa554fc26796d5e563adfbeba39d24b98',1,'UVC_HandleTypeDef']]]
];
