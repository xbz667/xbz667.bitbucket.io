var struct_u_v_c___handle_type_def =
[
    [ "__attribute__", "struct_u_v_c___handle_type_def.html#a4f75d312a8a7962e1d8a3d0b0eceb977", null ],
    [ "cfg_state", "struct_u_v_c___handle_type_def.html#afc51999d72b037ce9a8604f14b695f75", null ],
    [ "ctl_state", "struct_u_v_c___handle_type_def.html#a9fdbfe20e001b78fe72b3cab14846921", null ],
    [ "getParam_state", "struct_u_v_c___handle_type_def.html#a60676c6142f28cae9ae73eda87557efd", null ],
    [ "param", "struct_u_v_c___handle_type_def.html#aba18f3d521fde3af39c58b809a9c7ef0", null ],
    [ "probeSet", "struct_u_v_c___handle_type_def.html#a26615e600d5c905bd590946c94b09f1e", null ],
    [ "setParam_state", "struct_u_v_c___handle_type_def.html#aad70395b92a87dcac058ff222f168616", null ],
    [ "state", "struct_u_v_c___handle_type_def.html#aa5a1e131bc9b4c021a54ffb1b687e3dc", null ],
    [ "stream_altSetting", "struct_u_v_c___handle_type_def.html#a53ea2237c8deaacffb3d037fb49bbee2", null ],
    [ "stream_interface", "struct_u_v_c___handle_type_def.html#ac82570b8bd6991d8ce7cf4ad3c107dde", null ],
    [ "streamEp", "struct_u_v_c___handle_type_def.html#a953e438899d57fd11330340901b74efa", null ],
    [ "streamEp_addr", "struct_u_v_c___handle_type_def.html#a6dfd6a5e44a1ce6de700fe3ae5dc7f9c", null ],
    [ "streamLength", "struct_u_v_c___handle_type_def.html#a377f46fef1ef0309ae88034df6f10792", null ],
    [ "streamNeg_state", "struct_u_v_c___handle_type_def.html#a6285d36188e3f92697c630cac622f293", null ],
    [ "streamPipe", "struct_u_v_c___handle_type_def.html#a848d8a9aa16cdb7b2340cee9cae72b58", null ],
    [ "streamPoll", "struct_u_v_c___handle_type_def.html#aa554fc26796d5e563adfbeba39d24b98", null ]
];