var struct_wave_handler =
[
    [ "bytesRead", "struct_wave_handler.html#aaa6d06c7323523dd83c4a4f2b7d01217", null ],
    [ "currentSector", "struct_wave_handler.html#a12317a8320846fa34578920330756784", null ],
    [ "file", "struct_wave_handler.html#a296b3c88b34a2294d0bd990ac963543b", null ],
    [ "fileName", "struct_wave_handler.html#a88e8414787937e5cb2d811e517be5308", null ],
    [ "fileSize", "struct_wave_handler.html#a48f46ad02b77a00b795d15626ba1d42e", null ],
    [ "index", "struct_wave_handler.html#aafd95f8c7a99b9189ede7cdf0871ebe8", null ],
    [ "sector_size", "struct_wave_handler.html#a7d37def484362c6e97a2d75144080b1d", null ],
    [ "selector", "struct_wave_handler.html#a7cc715de833a0f4d62132e96541ee133", null ]
];