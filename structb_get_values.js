var structb_get_values =
[
    [ "backlight_comp", "structb_get_values.html#a4b5d2b541e394f60f5bb6440ef80ca4f", null ],
    [ "brightness", "structb_get_values.html#ace31b095fe77a4d4e0d3c66538807985", null ],
    [ "contrast", "structb_get_values.html#a4c6a47fd0d724c26246537e96a05c41a", null ],
    [ "gain", "structb_get_values.html#a7c07aa50dffd011a3b181ea4ecb54f8e", null ],
    [ "pwrline_freq", "structb_get_values.html#aa70d22300241953a5e7d185acb21c520", null ],
    [ "saturation", "structb_get_values.html#a4125fce2ae599832fcdd6903d31cca35", null ],
    [ "sharpness", "structb_get_values.html#a7d64ee1b05707f3c615d239a9330d8f5", null ]
];